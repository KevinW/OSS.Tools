using NUnit.Framework;
using OSS.Tools.Excel;
using OSS.Tools.Log;

namespace OSS.Tools.Tests.LogTests
{
    public class ExcelTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ExcelTest()
        {
            var file1Path = Path.Combine(AppContext.BaseDirectory, "ExcelTests", "TestLoadData.xlsx");

            using var stream = new FileStream(file1Path, FileMode.Open, FileAccess.Read);

            var list = stream.LoadListFromExcelFile<TestExcelMo>((i, r, msg) =>
            {
                if (msg != null)
                {
                    Console.WriteLine($"行：{i + 1}   转化数据错误：" + string.Join(',', msg.Select(d => d.title + ":" + d.err_msg)));
                }
                return true;

            }, 1, 2);

            Assert.IsTrue(list!=null && list.Any());

        }
    }




    public class TestExcelMo
    {
        [ExcelTitle("测试列1")]
        public int Col1 { get; set; }

        [ExcelTitle("时间字段1")]
        public DateTime? Time1 { get; set; }

        [ExcelTitle("结果")]
        public int Result { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ExcelTitleDictionary("TestDic")]
        public Dictionary<string, int> Dic { get; set; } = new Dictionary<string, int>();
    }
}